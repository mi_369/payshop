package com.ruoyi.goods.domain;

/**
 * Created by 魔金商城 on 17/7/14.
 * 单品的枚举
 */
public enum PmsSkuItem {
    SPEC, IMAGE, MEMBER_PRICE, BATCH
}
