package com.ruoyi.goods.vo;

/**
 * Created by 魔金商城 on 17/12/7.
 * 商品详情枚举
 */
public enum SpuDetailItem {
    COUPON, SPU_SPECS, SKU_COMMENT_NUM, FOLLOW, CATE, STORE_SCORE, BRAND, PC_DESC, MOBILE_DESC
}
